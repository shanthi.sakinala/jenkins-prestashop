# Aperçu

Jenkins est l’un des outils open source les plus utilisés pour effectuer les tâches liées à l’intégration continue.  
Parmi les tâches possibles on peut citer :
- L’automatisation des tâches récurrentes liées au développement, tests automatiques.
- Création des build, release.
- Déploiement de vos applications.

Dans ce projet nous allons construire une chaîne d'intégration et de déploiement automatique basée sur cet outil.  
Nous aurons l'occasion d'utiliser Docker pour construire l'image de CMS prestashop et Ansible pour déployer un conteneur docker sur le serveur de test.  
Nous déroulons aussi les tests unitaires et les tests d'acceptation avec Selenium.
  
# Structure de pipeline Jenkins

![jenkins pipeline](images/jenkins-pipeline.png)

Comme le montre le schéma, le pipeline est composé des stages suivants :
- **Satge Preparation :** clone le projet depuis gitLab et installe les composants logiciels du projet à l'aide de composer.
- **Stage Validate :** permet de valider la syntaxe des fichiers `yaml` et `twig`.
- **Satge Unit Tests :** déroule les tests unitaires et publie le rapport final sous format HTML.
- **Stage Quality :** vérifie la qualité de code en exécutant checkStyle, PMD et CPD, un rapport sera aussi publie
- **Stage Build :** construit l'image docker de prestashop et publie-la dans le rigistry gitLab.
- **Stage Deploy :** déploie une instance de prestashop sous format d'un conteneur docker, le déploiement sera réalisé à l'aide d'un plyabock Ansible.
- **Stage Acceptance Tests :** déroule les tests d'acceptation par le biais de l'outil Selenium.

# Création de l'infrastructure

Nous utilisons Vagrant pour créer l'infrastructure : 

![jenkins pipeline](images/infrastructure.png)

- VM jenkins (`192.168.34.40`) qui hébergera le serveur Jenkins.
- Vm prestashop (`192.168.34.50`) qui fera l'office d'un serveur de test.

Voici le script vagrant:

```sh
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "debian/buster64"
  config.vm.box_check_update = false
  config.vm.provider "virtualbox" do |vb|
    vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
    vb.customize ["modifyvm", :id, "--memory", 2048]
    vb.customize ["modifyvm", :id, "--cpus", "2"]
  end
  config.vm.define "jenkins" do |jenkins|
    jenkins.vm.hostname = "jenkins"
    jenkins.vm.network "forwarded_port", guest: 8080, host: 8080
    jenkins.vm.network "private_network", ip: "192.168.34.40"
    jenkins.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--memory", 4096]
    end
    jenkins.vm.provision :shell do |s|
      s.path = "install_docker.sh"
    end
    jenkins.vm.provision :shell do |s|
      s.path = "install_jenkins.sh"
    end
  end
  config.vm.define "prestashop" do |prestashop|
    prestashop.vm.hostname = "prestashop"
    prestashop.vm.network "forwarded_port", guest: 8090, host: 8090
    prestashop.vm.network "private_network", ip: "192.168.34.50"
    prestashop.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--memory", 2048]
    end
    prestashop.vm.provision :shell do |s|
      s.path = "install_docker.sh"
    end
  end
```
Script d'installation et configuration de docker.

```sh
sudo apt-get update
#Install docker and docker-compose
echo "[1]- Install docker and docker-compose"
sudo apt-get update
sudo apt-get install -qy docker.io docker-compose curl vim wget >/dev/null
sudo usermod -aG docker vagrant
newgrp docker
sudo systemctl enable docker
```
Script d'installation et configuration de Jenkins.
```sh
sudo ex +"%s@DPkg@//DPkg" -cwq /etc/apt/apt.conf.d/70debconf
sudo dpkg-reconfigure debconf -f noninteractive -p critical

sudo apt-get install -qy unzip zip git openjdk-11-jdk ansible >/dev/null

echo "[2]- Install Jenkins"
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get install -qy jenkins >/dev/null

# Add jenkins user to docker group
sudo usermod -aG docker jenkins
newgrp docker
sudo systemctl restart docker
sudo systemctl restart jenkins

# Install requirements for Prestashop
echo "[3]- Install requirements for Prestashop"
#apt-get install -y phpq php-bcmath php-cli php-curl php-zip php-sqlite3 php-mysql php-xml php-mbstring
sudo apt-get install -y php php-curl php-zip php-mbstring php-dom php-gd php-intl php-xdebug >/dev/null
wget -q https://getcomposer.org/composer.phar
sudo mv composer.phar /usr/bin/composer
chmod +x /usr/bin/composer

# Fix docker save credentials problem
sudo dpkg -r --ignore-depends=golang-docker-credential-helpers golang-docker-credential-helpers
```

# Configuration de Jenkins

## Démarrage 
Une fois l'exécution de script vagrant s'est bien passée, vous pouvez accéder à Jenkins via l'url [http://localhost:8080](http://localhost:8080).  
Suivrez les instructions de configuration de démarrage de Jenkins.  
Si vous voyez la fenêtre suivante, lorsque vous allez sur [http://localhost:8080](http://localhost:8080), pas de panique, tout va bien. Jenkins veut juste s’assurer qu’il s’agit bien d’un administrateur du poste sur le quel vous essayez de l’installer.  
Suivez les instructions, copiez et collez le mot de passe comme indiqué.

![page de lancement jenkins](images/start-jenkins.png)

Ensuite, Jenkins vous propose d'installer les plugins, soit en choisissant ceux par défaut ou en sélectionnant vous-mêmes les plugins que vous souhaitez installer.  
Vous pouvez effectuer le choix par défaut. Tout en sachant qu’il sera toujours possible d’ajouter des plugins une fois l’installation terminée.

![installation des plugins par defaut](images/installation-plugins.png)

Une fois les plugins installés, on vous propose de créer le premier utilisateur.

![créer le premier utilisateur](images/first-user.png)

Et à la fin, on vous propose de configurer une instance pour terminer l’installation.  
Ici vous êtes libre de modifier le port et mettre un port à votre convenance.

![lancement de l'instance jenkins](images/start-instance.png)

## Installation des plugins

Pour le bon fonctionnement de pipeline, vous devez installer un certain Plugin :

- **colver :** permet de traiter les rapports de couverture des tests unitaires. 
- **htmlpublisher :** permet de publier les rapports HTML générés au préalable par la phase de build ou la phase de test.
- **Warnings Next Generation :** ce plugin collecte et visualise les avertissements du compilateur ou les problèmes signalés par les outils d'analyse statique tel que checkStyle, PMD.
- **gitlab, gitlab api, gitlab authentication :** Ces plugins sont utilisés pour établir une communication avec le serveur GitLab, le build sera déclenché chaque fois qu'une modification de code est poussée.
- **docker pipline :** ce plugin permet d'intégrer docker dans le langage des pipelines Jenkins.
- **ansible :** ce plugin permet d'intégrer Ansible dans le langage des pipelines Jenkins.

Allez à la page : **Jenkins > Administrer Jenkins > Gestion des plugins**

![Gestion des plugins](images/gestion-plugins.png)

## Configuration des plugins.

### Plugin GitLab

Pour établir la connexion avec GitLab, vous aurez besoin d'ajouter une authentification par token.  
D'abord récupérer votre personnel GitLab token depuis GitLab.

Allez à la page:  **GitLab > User Setting > Access Token**
![GitLab token identification](images/GitLab-access-token.png)

Récupérez le token et créez une authentification par token au niveau de Jenkins.  
Allez à la page : **Jenkins > Administrer Jenkins > Manage Credentials > Jenkins > Identifiants globaux (illimité) > Ajouter des identifiants**  
Choisissez le type : `GitLab API token`.

![Jenkins GitLab API token](images/GitLab-Api-Token.png)

Maintenant, il faut configurer les paramètres de GitLab API au niveau de serveur Jenkins.  
Allez à la page : **Jenkins > Administrer Jenkins > Configurer le système**  
Ajoutez l'url de GitLab server et sélectionnez le token authentification crée précédemment :

![GitLab API configuration](images/GitLab-configuration.png)

N'oubliez pas de tester la connexion en cliquant sur le button `test connection`.

La connexion est établie, Jenkins a maintenant la possibilité de télécharger les metadata depuis GitLab, 
mais pour publier des changements depuis Jenkins vers GitLab vous aurez besoin d'une clef SSH.

Créer une clef SSH pour l'utilisateur Jenkins à l'aide de `ssh-keygen`.  
Rendez-vous à la page **GitLAb > User Setting > SSH keys**. Copier le contenu de la clef public `id_rsa.pub`

![Ajout de clef SSH au niveau de GitLab](images/GitLab-ssh-keys.png)

Maintenant, la clef publique est ajouté au GitLab serveur, il faut ensuite ajouter la clef privée au serveur Jenkins
Copiez le contenu du fichier `/home/jenkins/.ssh/id_rsa`

Allez à la page: **Jenkins > Administrer Jenkins > Manage Credentials > Jenkins > Identifiants globaux (illimité) > Ajouter des identifiants**

![Ajout d'identification de type SSH entre Jenkins et GitLab](images/Jenkins-private-key.png)

Sélectionnez `SSH Username with private key` comme type d'identifiant, le champ `Username` doit prendre la valeur git.

### Plugin Docker.

Pour publier vous images à votre préférable registry, vous devez configurer un moyen d'authentification entre Jenkins et le registry docker.
Pour mon cas j'utilise GitLab registry, il faut juste ajouter un identifiant de type `Nom d'utilisateur et mot de passe`.

![Créer un identifiant pour se connecter au GitLab registry](images/credentials-gitlab-registry.png)

Ce moyen d'authentification doit porter l'identifiant : `gitlab-credential` vu qu'on lui fait référence dans le script de pipeline.

### Plugin Ansible.

Ansible se base sur le protocole SSH pour réaliser les commandes à distance.  
Nous aurons alors besoin de mettre une communication SSH entre le serveur Jenkins et le serveur de test (`VM prstashop`).

Comme au préalable, vous devez créer un identifiant de type SSH au niveau Jenkins.  
Allez à la page : **Jenkins > Administrer Jenkins > Manage Credentials > Jenkins > Identifiants globaux (illimité) > Ajouter des identifiants**  
Sélectionnez le type d'identifiant : `SSH Username with private key`, le `Username` est vagrant.   
Ensuite ajoutez le contenu de la clef privée `/home/jenkins/.ssh/id_rsa` dans la section private key, et final ajouter un identifiant exemple `ansibe-private-key`

![Ajout d'identification de type SSH pour Ansible](images/Ansible-ssh-private-key.png)

# Pipeline Jenkins

## Creation de nouveau item type Pipeline.

La partie configuration est terminée, maintenant on peut passer à la création du pipeline Jenkins.  
Allez à la page : **Jenkins > Nouveau Item > Pipeline**  
Saisissez le nom de pipeline et validez.

![Céreation de nouveau Jenkins pipeline](images/create-Jenkins-pipeline.png)

La page qui suit concerne la configuration de pipeline.  
Ajoutez un paramètre au build de type choix qui sera dédié au choix de serveur de test. Pour notre cas on a un seul serveur : `192.168.34.50`

![Ajout de paramètres build au pipleine Jenkins](images/build-parameters.png)

Au niveau de la section pipeline, choisissez l'option `Pipline script from SCM` et `git` comme SCM.

![Pipeline Jenkins en utilisant Jenkinsfile](images/Pipeline-With-Jenkinsfile.png)

Il faut configurer le repository git comme suivant : 
- **Repository URL :** `git@gitlab.com:rsihammou/prestashop.git`
- **Credentials :** choisissez l'identifiant SSH crée préalablement pour git
Pour les autres champs, laissez les valeurs par défaut.

## Jenkinsfile.

Notre pipeline est de type script, qui se trouve à la racine de projet prestashop importé à mon compte GitLab pour avoir la possibilité d'effectuer des changements.   
Le dépôt original de projet prestashop est : [https://github.com/PrestaShop/PrestaShop](https://github.com/PrestaShop/PrestaShop)  

Le fichier Jenkinsfile décrit les différentes étapes constituant le pipeline :

```groovy
node {
def image = "registry.gitlab.com/rsihammou/jenkins-prestashop/prestashop"

    stage('Preparation') {
        sh 'rm -rf reports'
        sh 'mkdir reports'
        checkout scm
        sh 'composer install --optimize-autoloader --no-progress --no-interaction --no-ansi --no-scripts'
    }

    stage('Validate') {
        sh 'bash tests/check_file_syntax.sh'
    }

    stage('Unit Tests') {
        withEnv(["SYMFONY_DEPRECATIONS_HELPER=disabled"]) {
            sh 'vendor/bin/phpunit -c tests/Unit/phpunit.xml --coverage-clover=reports/coverage/coverage.xml --coverage-html=reports/coverage'
        }
        step([$class: 'CloverPublisher', cloverReportDir: 'reports/coverage', cloverReportFileName: 'coverage.xml'])
        publishHTML (target: [
            allowMissing: false,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: 'reports/coverage',
            reportFiles: 'index.html',
            reportName: "Coverage Report"
        ])
    }

    stage('Quality') {
        sh 'vendor/bin/phpcs src --report=checkstyle --report-file=reports/checkstyle.xml --standard=PSR2 --extensions=php || exit 0'
        sh 'vendor/bin/phpmd src/ xml phpmd.xml --reportfile reports/pmd.xml || exit 0'
        sh 'vendor/bin/phpcpd src/ --log-pmd reports/cpd.xml --exclude vendor || exit 0'
        recordIssues enabledForFailure: true, tools: [
            checkStyle(pattern: '**/reports/checkstyle.xml'),
            cpd(pattern: '**/reports/cpd.xml'),
            pmdParser(pattern: '**/reports/pmd.xml')
        ]
    }

    stage('Build') {
        docker.withRegistry('https://registry.gitlab.com', 'gitlab-credentials') {
            def dockerImage = docker.build("${image}:latest")
            /* Push the container to the custom Registry */
            dockerImage.push()
        }
    }

    stage('Deploy'){
        ansiblePlaybook(
            inventory: '${Host},',
            playbook: 'playbook.yml',
            credentialsId: 'ansible-private-key',
            disableHostKeyChecking: true
        )
    }

    stage('Acceptance Tests'){
      withEnv(["HOST_URL=http://${HOST}:8090"]) {
        sh 'vendor/bin/phpunit tests/Acceptance/installationTest.php'
      }
    }
}
```

### Stage Preparation:

Le but de ce stage et de préparer le terrain en créant le dossier `report` qui va contenir l'ensemble des rapports que ce soit ceux concernant la qualité de code ou ceux générés en exécutant les tests unitaires.  
Le checkout de code est effectué à ce niveau, ainsi que l'installation des différentes briques logicielles via composer.

### Stage Validate:

Permet de valider la syntaxe des fichiers `yaml` et `twig` inclus dans le projet prestashop.

### Stage Unit Tests:

Execute et génère le rapport des tests unitaires.
Le plugin `publishHTML` publie les rapports récupérés depuis le dossier `report` pour en créer un dashboard au niveau de Jenkins.

### Stage Quality

- **CheckStyle:** permet de vérifier un certain niveau de qualité de code, le respect des normes de développement ainsi que les bonnes pratiques facilitent grandement la lisibilité et la maintenabilité de code source.
- **PMD:** utilise un système de règles extensibles, il est capable de détecter le code mort, le code sur-compliqué en calculant sa complexité cyclomatique.  
  Vous pouvez définir vos propres règles suivant le besoin. Pour mon cas voici le fichier [phpmd.xml](phpmd.xml)
- **CPD:** détecte les parties de code dupliqué (un copier-coller par exemple)

Le plugin `Warnings Next Generation` publie les différents rapports générés pour en créer des dashboard visuels au niveau de Jenkins.

### Stage Build

L'image docker portant le nom `registry.gitlab.com/rsihammou/jenkins-prestashop/prestashop` version `latest` est construite à ce niveau, elle sera ensuite publiée dans le registry GitLab.  
L'authentification est assurée par le moyen d'identification `gitlab-credentials`.

[Dockerfile](Dockerfile)

```dockerfile
FROM prestashop/base:7.2-apache

ENV PS_VERSION 1.7.6.9

# Get PrestaShop
ADD https://www.prestashop.com/download/old/prestashop_1.7.6.9.zip /tmp/prestashop.zip

# Extract
RUN mkdir -p /tmp/data-ps \
&& unzip -q /tmp/prestashop.zip -d /tmp/data-ps/ \
&& bash /tmp/ps-extractor.sh /tmp/data-ps \
&& rm /tmp/prestashop.zip
```

### Stage Deploy 

Le déploiement d'une instance prestashop au sein de serveur du test est assurée par un playbook Ansible.

[playbook.yml](playbook.yml)

```yaml
---
- name: Install prestashop.
  hosts: all
  gather_facts: yes
  become: no
  tasks:
  - name: Copy docker-compose file
    copy:
    src: docker-compose.yml
    dest: docker-compose.yml

  - name: Remove the previous deployment
    command: docker-compose down --volumes

  - name: Run docker compose
    command: docker-compose up -d
```

La tache de playbook est de copier le fichier `docker-compose.yml` dans le serveur distant et de lancer son exécution. Une instance `prestashop` sera installée avec sa base de donnée sql, et elle sera accessible sur le port `8090`

[docker-compose.yml](docker-compose.yml)

```yaml
version: '2'
services:
  mysql:
    image: mysql:5.7
    ports:
      - "3307:3306"
    volumes:
      - db_data:/var/lib/mysql
    environment:
      MYSQL_DATABASE: prestashop
      MYSQL_ROOT_PASSWORD: prestashop
    restart: always
  prestashop:
    image: registry.gitlab.com/rsihammou/jenkins-prestashop/prestashop
    ports:
      - '8090:80'
    environment:
      DB_SERVER: mysql
      PS_LANGUAGE: fr
      PS_COUNTRY: FR
    volumes:
      - prestashop_data:/tmp/data-ps
    depends_on:
      - mysql
volumes:
  db_data:
    driver: local
  prestashop_data:
    driver: local
```

### Stage Acceptance Tests

Déroule les tests d'acceptation Selenium. Il faut lancer un serveur Selenium avant d'exécuter les tests.
Pour simplifier, on pourra utiliser la version standalone sous forme d'une image Docker :

```sh
docker run -d -p 4444:4444 -v /dev/shm:/dev/shm selenium/standalone-firefox:4.0.0-alpha-7-20201119
```

Le serveur Selenium est à l'écoute sur le port `4444`.

Le script déroule la phase d'installation de prestashop (Création de l'utilisateur, configuration de la base de données ...).
et au final il vérifié que la boutique a été bien crée et fait une simulation d'achat d'article.

[InstallationTest.php](tests/Acceptance/installationTest.php)

```php
<?php
require 'vendor/autoload.php';
use PHPUnit\Framework\TestCase;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverSelect;
use Facebook\WebDriver\WebDriverExpectedCondition;

class PrestashopTest extends TestCase {

    private static $driver;
    private static $host_url;
    private static $storeTitle = "maBoutique";

    /**
     * Open the firefox browser just once to be reused on each test.
     */
    public static function setupBeforeClass(): void
    {
        $host = 'http://localhost:4444/wd/hub'; // this is the default port
        $driver = RemoteWebDriver::create($host, DesiredCapabilities::firefox());
        self::$driver = $driver;
        self::$host_url = getenv('HOST_URL');
    }

    /**
     * Before each test the same browser instance will be reused,
     * so clear cookies, local storage, etc., or open a new incognito tab
     */
    public function setUp(): void {}

    /**
     * After all tests have finished, quit the browser,
     * even if an exception was thrown and/or tests fail
     */
    public static function tearDownAfterClass(): void
    {
        // Close the browser
        self::$driver->quit();
    }

    /**
     * Verify prestashop installation processus
     */
    public function testPrestashopInstallation()
    {
        $driver = self::$driver;
        $driver->get(self::$host_url.'/install/index.php');
        // Wait until the language drop-down list is fully loaded.
        $driver->wait(10)->until(
            function () use ($driver) {
                $select = new WebDriverSelect($driver->findElement(WebDriverBy::id('langList')));
                return count($select->getOptions()) == 44;
            },
            'Error unexpected number of items'
        );
        // Verify the page title
        $this->assertStringContainsString('PrestaShop Installation Assistant', $driver->getTitle());
        // Select English language
        $select = new WebDriverSelect($driver->findElement(WebDriverBy::id('langList')));
        $select->selectByValue('en');
        // Click on the next button
        $driver->findElement(WebDriverBy::id("btNext"))->click();
        // Wait until the license page is fully loaded
        $driver->wait(10)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::id('licenses-agreement'))
        );
        // Verify the form title
        $this->assertStringContainsString('License Agreements',
                                          $driver->findElement(WebDriverBy::id("licenses-agreement"))
                                                 ->getText());
        // Validate license checkbox
        $driver->findElement(WebDriverBy::id("set_license"))->click();
        // Go to the next page
        $driver->findElement(WebDriverBy::id("btNext"))->click();
        // Wait until the info store page is fully loaded
        $driver->wait(10)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::id('infosShopBlock'))
        );
        // Verify the form title
        $this->assertStringContainsString('Information about your Store',
                                          $driver->findElement(WebDriverBy::cssSelector("#infosShopBlock h2"))
                                                 ->getText());
        // Add shop name
        $driver->findElement(WebDriverBy::id("infosShop"))->sendKeys(self::$storeTitle);
        // Choose shop activity
        $driver->findElement(WebDriverBy::cssSelector("#infosActivity_chosen span"))->click();
        $driver->findElement(WebDriverBy::cssSelector("#infosActivity_chosen .active-result:nth-child(3)"))->click();
        // Choose shop country
        $driver->findElement(WebDriverBy::cssSelector("#infosCountry_chosen span"))->click();
        $driver->findElement(WebDriverBy::cssSelector("#infosCountry_chosen .active-result:nth-child(2)"))->click();
        // Admin conf
        $driver->findElement(WebDriverBy::id("infosFirstname"))->sendKeys("admin");
        $driver->findElement(WebDriverBy::id("infosName"))->sendKeys("admin");
        $driver->findElement(WebDriverBy::id("infosEmail"))->sendKeys("admin@mail.fr");
        $driver->findElement(WebDriverBy::id("infosPassword"))->sendKeys("password");
        $driver->findElement(WebDriverBy::id("infosPasswordRepeat"))->sendKeys("password");
        // Go to the next page
        $driver->findElement(WebDriverBy::id("btNext"))->click();
        // Wait until the database configuration page is fully loaded
        $driver->wait(10)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::id('formCheckSQL'))
        );
        // Verify the form title
        $this->assertStringContainsString('Configure your database by filling out the following fields',
                                          $driver->findElement(WebDriverBy::cssSelector("#dbPart h2"))
                                                 ->getText());
        // Add database server
        $driver->findElement(WebDriverBy::id("dbServer"))->clear();
        $driver->findElement(WebDriverBy::id("dbServer"))->sendKeys("mysql");
        // Add database password
        $driver->findElement(WebDriverBy::id("dbPassword"))->sendKeys("prestashop");
        // Check database connection
        $driver->findElement(WebDriverBy::id("btTestDB"))->click();
        $driver->wait(10)->until(
            WebDriverExpectedCondition::elementTextIs(WebDriverBy::id('dbResultCheck'), 'Database is connected')
        );

        // Validate installation
        $driver->findElement(WebDriverBy::id("btNext"))->click();
        // Wait until completed installation
        $driver->wait(300)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::id('install_process_success'))
        );
        // Check installation status
        $this->assertStringContainsString('Your installation is finished!',
                                          $driver->findElement(WebDriverBy::cssSelector("#install_process_success .clearfix h2"))
                                                 ->getText());
    }

    /**
     * Verify store
     */
    public function testMyStore()
    {
        $driver = self::$driver;
        $driver->get(self::$host_url.'/en/');
        // Wait until the index page is fully loaded and check the title
        $driver->wait(10)->until(
            WebDriverExpectedCondition::titleIs(self::$storeTitle)
        );
        // Check that the page contain 8 products
        $products = $driver->findElements(WebDriverBy::xpath("//article[contains(@class, 'product-miniature')]"));
        $this->assertCount(8, $products);
        // Click on the first product
        $driver->findElement(WebDriverBy::cssSelector(".product-miniature:nth-child(1) img"))->click();

        // Wait until the index page is fully loaded and check the title
        $driver->wait(10)->until(
            WebDriverExpectedCondition::titleIs("Hummingbird printed t-shirt")
        );

        // Select medium size
        $select = new WebDriverSelect($driver->findElement(WebDriverBy::id('group_1')));
        $select->selectByValue('2');
        // Select black color
        $driver->findElement(WebDriverBy::cssSelector(".float-xs-left:nth-child(2) .input-color"))->click();
        // Change product quantity
        $driver->findElement(WebDriverBy::cssSelector(".touchspin-up"))->click();
        // Add product to the cart
        $driver->findElement(WebDriverBy::cssSelector(".add-to-cart"))->click();

        // Wait until the cart modal will be displayed
        $driver->wait(10)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::id("blockcart-modal"))
        );

        // Check the total of chosen products
        $this->assertStringContainsString('There are 2 items in your cart.',
                                           $driver->findElement(WebDriverBy::xpath("//div[@class='cart-content']/p[1]"))
                                                  ->getText());
        // Check the price
        $total = $driver->findElement(WebDriverBy::xpath("//div[@class='cart-content']/p[4]/span[2]"))->getText();
        $this->assertStringContainsString('€45.89',  $total);
    }
}
?>
```

# Lancement de pipeline

Vous pouvez lancer le pipeline manuellement en se rendant à la page :  
**Jenkins > [votre pipeline] > Lancer un build avec des paramètres**  

![Build Jenkins pipeline](images/build-pipeline.png)

Sélectionnez l'adresse IP de serveur de test, et ensuite cliquez sur build.


![Jenkins pipeline avec build paramètres](images/select-build-parameter.png)

Le build se lance automatiquement, il déroulera l'ensemble des stages d'une façon séquentielle.  
Une fois l'opération terminée, vous pouvez accéder au différents dashboards.

### Code coverage dashboard :

![Couverture de code](images/code-coverage.png)

![Rapport de couverture de code](images/coverage-report-summary.png)

### PMD dashboard:

![Rapport PMD](images/PMD-report.png)

### CPD dashboard :

![Rapport CPD](images/CPD-report.png)

### CheckStyle dashboard :

![Rapport checkStyle](images/checkStyle-report.png)
