import unittest
from selenium import webdriver

from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

import time
from time import sleep
 
class GoogleSeachTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub', desired_capabilities=DesiredCapabilities.FIREFOX) 
    def test_GoogleSearch(self):
        driver_firefox = self.driver
       # driver_firefox.maximize_window()
        driver_firefox.get('http://www.google.com')
 
        # Perform search operation
        elem = driver_firefox.find_element_by_name("q")
        elem.send_keys("Lambdatest")
        elem.submit()
 
        sleep(10)
 
    def tearDown(self):
        # Close the browser.
        self.driver.quit()
 
if __name__ == '__main__':
    unittest.main()
