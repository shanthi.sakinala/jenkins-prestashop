import unittest
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import time
from time import sleep
 
class WikipediaSeachTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub', desired_capabilities=DesiredCapabilities.FIREFOX)

    def test_WikipediaSearch(self):
        driver_firefox = self.driver
        driver_firefox.maximize_window()
 
 
        # Perform search operation
        driver_firefox.get('http://en.wikipedia.org')
 
        driver_firefox.find_element_by_id('searchInput').clear()
        driver_firefox.find_element_by_id('searchInput').send_keys('Steve Jobs')
 
        sleep(10)
        driver_firefox.find_element_by_id('searchButton').click()
 
        sleep(10)
 
    def tearDown(self):
        # Close the browser.
        self.driver.close()
 
if __name__ == '__main__':
    unittest.main()
