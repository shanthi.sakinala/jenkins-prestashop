sudo ex +"%s@DPkg@//DPkg" -cwq /etc/apt/apt.conf.d/70debconf
sudo dpkg-reconfigure debconf -f noninteractive -p critical

sudo apt-get install -qy unzip zip git openjdk-11-jdk ansible >/dev/null

echo "[2]- Install Jenkins"
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get install -qy jenkins >/dev/null

# Add jenkins user to docker group
sudo usermod -aG docker jenkins
newgrp docker
sudo systemctl restart docker
sudo systemctl restart jenkins

# Install requirements for Prestashop
echo "[3]- Install requirements for Prestashop"
#apt-get install -y phpq php-bcmath php-cli php-curl php-zip php-sqlite3 php-mysql php-xml php-mbstring
sudo apt-get install -y php php-curl php-zip php-mbstring php-dom php-gd php-intl php-xdebug >/dev/null
wget -q https://getcomposer.org/composer.phar
sudo mv composer.phar /usr/bin/composer
chmod +x /usr/bin/composer

# Fix docker save credentials problem
sudo dpkg -r --ignore-depends=golang-docker-credential-helpers golang-docker-credential-helpers

